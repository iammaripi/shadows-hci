# **Shadows of IPO**

Este es el proyecto para la asignatura Interacción Persona-Ordenador II de la Escuela Superior de Informática de Ciudad Real [ESI](http://webpub.esi.uclm.es/).

### **¿Qué es Shadows of IPO?** ###

Es un juego de destreza visual donde el usuario deberá emparejar una imagen con su sombra correspondiente. Está desarrollado en lenguaje C# y XAML con la ayuda de Visual Studio y Expression Blend. 

### **Documentación** ###

Puede encontrar toda la documentación necesaria en las diferentes secciones de la [wiki del proyecto](https://bitbucket.org/iammaripi/shadows-hci/wiki/Home).