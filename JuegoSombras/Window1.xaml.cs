﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace JuegoSombras
{
	/// <summary>
	/// Lógica de interacción para Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{

        public List<Jugador> jugs;
		int intentos= 2;
        DispatcherTimer timer;
		Boolean pulsado= false;
		
        public Window1(List<Jugador> jugadores)
		{
			this.InitializeComponent();
            jugs = jugadores;
			inicializarGrids();
			gridNivel1.Visibility = Visibility.Visible;
			gridFinJuego.Visibility = Visibility.Hidden;
			lblNIntentos.Content = intentos;	
			tbTemporizador.Text = "15";
			timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(eventoTiempo);
			timer.Start();
		}

		//Evento para que al cerrar la ventana se guarden los datos del jugador en el .txt
        private void Window_Closed(object sender, EventArgs e)
        {
            string miEscritorio = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            StringBuilder sb = new StringBuilder();
            sb.Append(jugs[jugs.Count - 1].getNick() + "        " + jugs[jugs.Count - 1].getPuntuacion());
            using (StreamWriter outfile = new StreamWriter("PuntuacionesJugadores.txt", true))
            {
                outfile.WriteLine(sb.ToString());
            }
            this.Close();
        }
		
		//Evento generico para el dragdrop de las imagenes que hay que arrastrar, ya que no hay
		//que particularizar por cada una
		private void imgOriginal_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			DataObject dataO = new DataObject(((Image)sender));
            DragDrop.DoDragDrop((Image)sender, dataO, DragDropEffects.Move);
		}
		
		//Metodo para sumar puntos al jugador y reiniciar nivel
		private void acierto(){
			imgSumar.Visibility = Visibility.Visible;
			DoubleAnimation widthAnimation = new DoubleAnimation(58.224, 0, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = false;
            imgSumar.BeginAnimation(Image.WidthProperty, widthAnimation);
			sonarAcierto();
            jugs[jugs.Count - 1].setPuntuacion(jugs[jugs.Count - 1].getPuntuacion() +5);
            lblPuntuacion.Content = jugs[jugs.Count - 1].getPuntuacion();
			reiniciarTemporizador();
			btnPista.IsEnabled= true;
			pbProgreso.Foreground = Brushes.White;
			pbProgreso.BorderBrush = Brushes.Gray;
		}
		
		//Metodo para restar intentos
		private void restarIntentos (){
			if(intentos >0){
				intentos--;
				lblNIntentos.Content = intentos;
				sonarCuervo();
			}
			else{
				intentos--;
				finDelJuego();
			}
		}
		
		//Evento para mostrar una pista
		private void btnPista_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			jugs[jugs.Count - 1].setPuntuacion(jugs[jugs.Count - 1].getPuntuacion() -2);
            lblPuntuacion.Content = jugs[jugs.Count - 1].getPuntuacion();
			btnPista.IsEnabled= false;
			if (gridNivel1.Visibility == Visibility.Visible) imgOpcion4Nivel1.Visibility = Visibility.Hidden;
			else if (gridNivel2.Visibility == Visibility.Visible) imgOpcion1Nivel2.Visibility = Visibility.Hidden;
			else if (gridNivel3.Visibility == Visibility.Visible) imgOpcion3Nivel3.Visibility = Visibility.Hidden;
			else if (gridNivel4.Visibility == Visibility.Visible) imgOpcion1Nivel4.Visibility = Visibility.Hidden;
			else if (gridNivel5.Visibility == Visibility.Visible) imgOpcion4Nivel5.Visibility = Visibility.Hidden;
			else if (gridNivel6.Visibility == Visibility.Visible) imgOpcion2Nivel6.Visibility = Visibility.Hidden;
			else if (gridNivel7.Visibility == Visibility.Visible) imgOpcion4Nivel7.Visibility = Visibility.Hidden;
			else if (gridNivel8.Visibility == Visibility.Visible) imgOpcion3Nivel8.Visibility = Visibility.Hidden;
		}
		
		//Metodo que finaliza el juego
		private void finDelJuego(){
			inicializarGrids();
			if(intentos>=0) lblFin.Content= "      Has ganado\n    con "+ jugs[jugs.Count - 1].getPuntuacion() + " puntos\n   ¿Volver a jugar?";
			lblArrastre.Visibility = Visibility.Hidden;
			pbProgreso.Visibility = Visibility.Hidden;
			tbTemporizador.Visibility = Visibility.Hidden;
			gridFinJuego.Visibility = Visibility.Visible;
			btnPista.Visibility = Visibility.Hidden;
			timer.Stop();
		}

		//Eventos de botones finales
		private void btnSi_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
		}

		private void btnNo_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			MessageBox.Show("Gracias por jugar.\nTu puntuación se guardará por si quieres revisarla y superarla la próxima vez.", "¡Hasta luego!", MessageBoxButton.OK, MessageBoxImage.Information);
			App.Current.Shutdown();
		}
		
		
		//Evento que controla el tiempo
		private void eventoTiempo(object sender, EventArgs e)
        {
			//inicializamos un contador con el numero que haya en el textbox por si
			//queremos cambiarlo, que sea desde la capa de presentacion y no afecte al codigo
            int contador = Convert.ToInt32(tbTemporizador.Text);
            if (contador != 0)
            {
                contador--;
                tbTemporizador.Text = contador.ToString();
				pbProgreso.Value= pbProgreso.Value-6.66;
				if(pbProgreso.Value<40) {
					pbProgreso.Foreground = Brushes.Red;
					pbProgreso.BorderBrush = Brushes.Red;
				}
            }
            else
            {
                timer.Stop();
                finDelJuego();
            }
        }
		
		//Metodo para reiniciar el temporizador y la barra de progreso
		private void reiniciarTemporizador(){
			timer.Stop();
			pbProgreso.Value= 100;
			tbTemporizador.Text = "15";
			timer.Start();
		}

		//Evento que para o reanuda el sonido
		private void imgSound2_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(pulsado==false){
				pulsado=true;
        		sonidoFondo2.LoadedBehavior = MediaState.Pause;
				imgSound2.Source = new BitmapImage(new Uri(@"volume_up-26.png", UriKind.Relative));
			}
			else{
				pulsado=false;
				sonidoFondo2.LoadedBehavior = MediaState.Play;
				imgSound2.Source = new BitmapImage(new Uri(@"mute-26.png", UriKind.Relative));
			}
		}

		//Evento que inicializa el sonido de fondo una vez acabe para volver a empezar
		private void sonidoFondo_MediaEnded(object sender, System.Windows.RoutedEventArgs e)
		{
			sonidoFondo2.Position = TimeSpan.Zero;
   			sonidoFondo2.LoadedBehavior = MediaState.Play;
		}
		
		//Metodos de sonidos de acierto y fallo
		private void sonarCuervo(){
			//para que no suene nada mas abrir el juego se mutea, luego se quita
			sonidoCuervo.IsMuted = false;
			sonidoCuervo.LoadedBehavior = MediaState.Play;
			sonidoCuervo.Position = TimeSpan.Zero; //iniciamos de nuevo el sonido para otra vez
		}
		
		private void sonarAcierto(){
			//para que no suene nada mas abrir el juego se mutea, luego se quita
			sonidoAcierto.IsMuted = false;
			sonidoAcierto.LoadedBehavior = MediaState.Play;
			sonidoAcierto.Position = TimeSpan.Zero; //iniciamos de nuevo el sonido para otra vez
			System.Threading.Thread.Sleep(1000); //Esperamos para mostrar el siguiente grid
		}
		
		//Metodo que pone los grids a Hidden para comenzar el juego y al acabar
		private void inicializarGrids(){
			gridNivel1.Visibility = Visibility.Hidden;
			gridNivel2.Visibility = Visibility.Hidden;
			gridNivel3.Visibility = Visibility.Hidden;
			gridNivel4.Visibility = Visibility.Hidden;
			gridNivel5.Visibility = Visibility.Hidden;
			gridNivel6.Visibility = Visibility.Hidden;
			gridNivel7.Visibility = Visibility.Hidden;
			gridNivel8.Visibility = Visibility.Hidden;
		}

		//Eventos del NIVEL 1
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion3Nivel1(){
			acierto();
            gridNivel1.Visibility = Visibility.Hidden;
            gridNivel2.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion1Nivel1(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel1.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel1.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion2Nivel1(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel1.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel1.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel1(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel1.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel1.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		//Eventos que llaman a los compartidos
        private void imgOpcion3Nivel1_MouseUp(object sender, MouseButtonEventArgs e)
        {
			eventoOpcion3Nivel1();
        }
		private void imgOpcion3Nivel1_Drop(object sender, System.Windows.DragEventArgs e)
        {
			eventoOpcion3Nivel1();
        }
		
        private void imgOpcion1Nivel1_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion1Nivel1();
        }
		private void imgOpcion1Nivel1_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel1();
		}
		
        private void imgOpcion2Nivel1_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion2Nivel1();
        }
		private void imgOpcion2Nivel1_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel1();
		}
		
		private void imgOpcion4Nivel1_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion4Nivel1();
        }
		private void imgOpcion4Nivel1_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel1();
		}
		
		//Eventos del NIVEL 2
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion2Nivel2(){
			acierto();
        	gridNivel2.Visibility = Visibility.Hidden;
            gridNivel3.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion1Nivel2(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel2.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel2.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel2(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel2.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel2.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel2(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel2.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel2.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		//Eventos que llaman a los compartidos
        private void imgOpcion2Nivel2_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			eventoOpcion2Nivel2();
        }
		private void imgOpcion2Nivel2_Drop(object sender, DragEventArgs e)
        {
			eventoOpcion2Nivel2();
        }
		
		private void imgOpcion1Nivel2_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion1Nivel2();
        }
		private void imgOpcion1Nivel2_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel2();
		}
		
        private void imgOpcion3Nivel2_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion3Nivel2();
        }
		private void imgOpcion3Nivel2_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel2();
		}

        private void imgOpcion4Nivel2_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion4Nivel2();
        }
		private void imgOpcion4Nivel2_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel2();
		}

		//Eventos del NIVEL 3
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion1Nivel3(){
			acierto();
        	gridNivel3.Visibility = Visibility.Hidden;
            gridNivel4.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion2Nivel3(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel3.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel3.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel3(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel3.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel3.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel3(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel3.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel3.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		//Eventos que llaman a los compartidos
        private void imgOpcion1Nivel3_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			eventoOpcion1Nivel3();
        }
        private void imgOpcion1Nivel3_Drop(object sender, DragEventArgs e)
        {
			eventoOpcion1Nivel3();
        }

        private void imgOpcion2Nivel3_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion2Nivel3();
        }
		private void imgOpcion2Nivel3_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			 eventoOpcion2Nivel3();
		}
		
        private void imgOpcion3Nivel3_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion3Nivel3();
        }
		private void imgOpcion3Nivel3_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel3();
		}
		
        private void imgOpcion4Nivel3_Drop(object sender, DragEventArgs e)
        {
            eventoOpcion4Nivel3();
        }
		private void imgOpcion4Nivel3_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel3();
		}
		
		//Eventos del NIVEL 4
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion4Nivel4(){
			acierto();
        	gridNivel4.Visibility = Visibility.Hidden;
            gridNivel5.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion1Nivel4(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel4.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel4.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion2Nivel4(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel4.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel4.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel4(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel4.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel4.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		//Eventos que llaman a los compartidos
		private void imgOpcion4Nivel4_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion4Nivel4();
		}
		private void imgOpcion4Nivel4_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel4();
		}
		
		private void imgOpcion1Nivel4_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion1Nivel4();
		}
		private void imgOpcion1Nivel4_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel4();
		}
		
		private void imgOpcion2Nivel4_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion2Nivel4();
		}
		private void imgOpcion2Nivel4_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel4();
		}
		
		private void imgOpcion3Nivel4_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion3Nivel4();
		}
		private void imgOpcion3Nivel4_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel4();
		}

		//Eventos del NIVEL 5
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion2Nivel5(){
			acierto();
        	gridNivel5.Visibility = Visibility.Hidden;
            gridNivel6.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion1Nivel5(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel5.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel5.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel5(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel5.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel5.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel5(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel5.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel5.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		//Eventos que llaman a los compartidos
		private void imgOpcion2Nivel5_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion2Nivel5();
		}
		private void imgOpcion2Nivel5_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel5();
		}

		private void imgOpcion1Nivel5_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion1Nivel5();
		}
		private void imgOpcion1Nivel5_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel5();
		}
		
		private void imgOpcion3Nivel5_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion3Nivel5();
		}
		private void imgOpcion3Nivel5_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel5();
		}
		
		private void imgOpcion4Nivel5_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion4Nivel5();
		}
		private void imgOpcion4Nivel5_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel5();
		}
		
		//Eventos del NIVEL 6
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion3Nivel6(){
			acierto();
        	gridNivel6.Visibility = Visibility.Hidden;
            gridNivel7.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion1Nivel6(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel6.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel6.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion2Nivel6(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel6.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel6.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel6(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel6.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel6.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		//Eventos que llaman a los compartidos
		private void imgOpcion3Nivel6_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion3Nivel6();
		}
		private void imgOpcion3Nivel6_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel6();
		}
		
		private void imgOpcion2Nivel6_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion2Nivel6();
		}
		private void imgOpcion2Nivel6_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel6();
		}

		private void imgOpcion1Nivel6_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion1Nivel6();
		}
		private void imgOpcion1Nivel6_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel6();
		}
		
		private void imgOpcion4Nivel6_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion4Nivel6();
		}
		private void imgOpcion4Nivel6_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel6();
		}
		
		//Eventos del NIVEL 7
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion1Nivel7(){
			acierto();
        	gridNivel7.Visibility = Visibility.Hidden;
            gridNivel8.Visibility = Visibility.Visible;
		}
		
		private void eventoOpcion2Nivel7(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel7.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel7.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel7(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel7.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel7.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion4Nivel7(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion4Nivel7.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion4Nivel7.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		//Eventos que llaman a los compartidos
		private void imgOpcion3Nivel7_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion3Nivel7();
		}
		private void imgOpcion3Nivel7_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel7();
		}
		
		private void imgOpcion2Nivel7_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion2Nivel7();
		}
		private void imgOpcion2Nivel7_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel7();
		}

		private void imgOpcion1Nivel7_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion1Nivel7();
		}
		private void imgOpcion1Nivel7_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel7();
		}
		
		private void imgOpcion4Nivel7_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion4Nivel7();
		}
		private void imgOpcion4Nivel7_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel7();
		}
		
		//Eventos del NIVEL 8
		//Eventos compartidos para no repetir codigo
		private void eventoOpcion4Nivel8(){
			acierto();
			finDelJuego();
		}
		
		private void eventoOpcion2Nivel8(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion2Nivel8.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion2Nivel8.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion3Nivel8(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion3Nivel8.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion3Nivel8.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		private void eventoOpcion1Nivel8(){
			DoubleAnimation widthAnimation = new DoubleAnimation(imgOpcion1Nivel8.ActualWidth, 1, TimeSpan.FromSeconds(0.5));
            widthAnimation.AutoReverse = true;
            imgOpcion1Nivel8.BeginAnimation(Image.WidthProperty, widthAnimation);
			restarIntentos();
		}
		
		//Eventos que llaman a los compartidos
		private void imgOpcion3Nivel8_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion3Nivel8();
		}
		private void imgOpcion3Nivel8_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion3Nivel8();
		}
		
		private void imgOpcion2Nivel8_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion2Nivel8();
		}
		private void imgOpcion2Nivel8_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion2Nivel8();
		}

		private void imgOpcion1Nivel8_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion1Nivel8();
		}
		private void imgOpcion1Nivel8_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion1Nivel8();
		}
		
		private void imgOpcion4Nivel8_Drop(object sender, System.Windows.DragEventArgs e)
		{
			eventoOpcion4Nivel8();
		}
		private void imgOpcion4Nivel8_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			eventoOpcion4Nivel8();
		}

	}//Fin
}