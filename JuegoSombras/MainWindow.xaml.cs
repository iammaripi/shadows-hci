﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JuegoSombras
{
	/// <summary>
	/// Lógica de interacción para MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        public List<Jugador> jugadores;
        Boolean pulsado= false;
		Boolean pulsado2= false;
		Boolean pulsado3= false;
		int elementos=0;
		
		public MainWindow()
		{
			this.InitializeComponent();
           jugadores=  new List<Jugador>();
           leerFicheroCargarPuntuaciones();
		}

		//Eventos de focus del nick
		private void txtNick_GotFocus(object sender, System.Windows.RoutedEventArgs e)
		{
			if (txtNick.Text == "JugDefecto") txtNick.Text = "";
			txtNick.Foreground= Brushes.Black;
		}
		private void txtNick_LostFocus(object sender, System.Windows.RoutedEventArgs e)
		{
			if (txtNick.Text == "") {
				txtNick.Text = "JugDefecto";
				txtNick.Foreground= Brushes.Gray;
			}
		}

		//Evento boton jugar
		private void btnJugar_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            jugadores.Add(new Jugador(txtNick.Text, 0));
            Window1 ventanaJuego = new Window1(jugadores);
            ventanaJuego.Show();
			ventanaJuego.lblUsuario.Content = jugadores[jugadores.Count-1].getNick();
            ventanaJuego.lblPuntuacion.Content = jugadores[jugadores.Count-1].getPuntuacion();
            
			this.Close();
		}
		
		//Evento para leer del fichero los datos de los jugadores anteriores y cargarlos al sp
        private void leerFicheroCargarPuntuaciones()
        {
            try
            {
				using (StreamReader sr = new StreamReader("PuntuacionesJugadores.txt"))
                {
					String linea= "";
					while(linea!=null){
						linea = sr.ReadLine();
						Label aux = new Label();
						aux.Content = linea;
						aux.Foreground = Brushes.White;
						this.spPuntuaciones.Children.Add(aux);
						elementos++;
						if(elementos>=10){ //si no entran mas jugadores borramos el mas antiguo
							spPuntuaciones.Children.RemoveAt(0);
						}
					}
                }
            }
            catch (Exception ex)
            {
                Label aux = new Label();
                aux.Content = "Could not read the file";
            }   
        }

		//Evento boton estadisticas de jugadores
        private void btnEstadisticas_Click(object sender, System.Windows.RoutedEventArgs e)
        {
			if(pulsado==false){
				pulsado=true;
        		spPuntuaciones.Visibility = Visibility.Visible;
			}
			else{
				pulsado=false;
				spPuntuaciones.Visibility = Visibility.Hidden;
			}
        }
		
		//Evento boton ayuda
		private void btnAyuda_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if(pulsado2==false){
				pulsado2=true;
        		spComoJugar.Visibility = Visibility.Visible;
				Label aux = new Label();
				aux.Foreground = Brushes.White;
                aux.Content = "¿Cómo jugar?\nEl objetivo del juego consiste\nen encontrar la sombra que\nse acople a la imagen dada.\nSólo hay que arrastrar la\nimagen hacia la sombra o\npinchar en la sombra.\nRecuerda que sólo tienes\npermitidos dos fallos.\n¡El tiempo corre!\n¿Hasta qué nivel serás capaz\nde llegar?";
                this.spComoJugar.Children.Add(aux);
			}
			else{
				pulsado2=false;
				spComoJugar.Visibility = Visibility.Hidden;
				this.spComoJugar.Children.Clear();
			}
        }

		//Eventos de sonido de fondo
        private void sonidoFondo_MediaEnded(object sender, System.Windows.RoutedEventArgs e)
        {
   			sonidoFondo.Position = TimeSpan.Zero;
   			sonidoFondo.LoadedBehavior = MediaState.Play;
        }
		
        private void imgSound_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        	if(pulsado3==false){
				pulsado3=true;
        		sonidoFondo.LoadedBehavior = MediaState.Pause;
				imgSound.Source = new BitmapImage(new Uri(@"volume_up-26.png", UriKind.Relative));
			}
			else{
				pulsado3=false;
				sonidoFondo.LoadedBehavior = MediaState.Play;
				imgSound.Source = new BitmapImage(new Uri(@"mute-26.png", UriKind.Relative));
			}
        }
		//Fin
	}
}