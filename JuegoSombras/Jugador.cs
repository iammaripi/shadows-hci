﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JuegoSombras
{
	public class Jugador
	{
		private String nick;
		private int puntuacion;
		
		public Jugador(String nick, int puntuacion)
		{
			this.nick= nick;
			this.puntuacion=puntuacion;
		}
		
		public String getNick() {
			return nick;
		}
		public void setNick(String nick) {
			this.nick = nick;
		}
		
		public int getPuntuacion() {
			return puntuacion;
		}
		public void setPuntuacion(int puntuacion) {
			this.puntuacion = puntuacion;
		}
		
	}
}